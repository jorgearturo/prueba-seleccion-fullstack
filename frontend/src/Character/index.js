import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import CharCard from '../CharCard';
import PropTypes from 'prop-types';
import styles from './styles.css';

function genderInterface(gender) {
  if (gender === 'male') return 'Masculino';
  if (gender === 'female') return 'Femenino';
  return '';
}

const Character = (props) => {
  Character.propTypes = {
    router: PropTypes.object,
  };
  const history = useHistory();
  const { router } = props;
  const [character, setCharacter] = useState();

  let params = new URLSearchParams(router.location.search);
  let searchId = params.get('id');

  useEffect(() => {
    fetch(`http://localhost:4000/characters?id=${searchId}`, { mode: 'cors' })
      .then((response) => response.json())
      .then((character) => {
        setCharacter(character);
      });
  }, [searchId]);
  return (
    <div>
      {character && (
        <div className="character-display-container">
          <CharCard name={character.name} image={character.image} />
          <div className="character-profile">
            <div className="grid">
              <div>
                <p>Sexo: </p>
                <p>{genderInterface(character.gender)}</p>
              </div>
              <div>
                <p>Slug: </p>
                <p>{character.slug}</p>
              </div>
              <div>
                <p>Rango: </p>
                <p>{character.pagerank.rank}</p>
              </div>
            </div>
            <div className="grid">
              <div>
                <p>Casa: </p>
                <p>{character.house || 'Sin Casa'}</p>
              </div>
              <div>
                <p>Libros: </p>
                <p>{character.books.length || 0}</p>
              </div>
              <div>
                <p>Titulos: </p>
                <p>{character.titles.length || 0}</p>
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="button-container">
        <button onClick={() => history.push('/')}>Volver</button>
      </div>
    </div>
  );
};

export default Character;
