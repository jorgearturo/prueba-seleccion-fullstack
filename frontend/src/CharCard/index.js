import React from 'react';
import PropTypes from 'prop-types';

const CharCard = (props) => {
  CharCard.propTypes = {
    image: PropTypes.string,
    name: PropTypes.string,
    onClick: PropTypes.func,
  };

  const { image, name, onClick } = props;

  return (
    <div className="character" onClick={onClick}>
      <img className="character-image" src={image} />
      {!image && <p className="no-img">Imagen no disponible</p>}
      <div className="description-card">
        <p>{name} </p>
      </div>
    </div>
  );
};

export default CharCard;
