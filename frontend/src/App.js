import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Pagination from 'react-js-pagination';
import CharCard from './CharCard';

function App() {
  const [characters, setCharacters] = useState();
  const [displayedCharacters, setDisplayedCharacters] = useState();
  const [actualPage, setActualPage] = useState(0);
  const history = useHistory();

  function handlePageChange(n) {
    setActualPage(n);
    const pageCharacters = characters.slice(n * 10 - 10, n * 10);
    setDisplayedCharacters(pageCharacters);
  }

  function handleSearch(event) {
    const filteredData = characters.filter((character) => {
      const house = character.house || 'Sin Casa';
      return (
        character.name.toLowerCase().includes(event.target.value) ||
        house.includes(event.target.value)
      );
    });
    setDisplayedCharacters(filteredData);
  }

  useEffect(() => {
    fetch('http://localhost:4000/characters', { mode: 'cors' })
      .then((response) => response.json())
      .then((characters) => {
        setCharacters(characters);
        const pageCharacters = characters.slice(0, 10);
        setDisplayedCharacters(pageCharacters);
      });
  }, []);

  return (
    <div className="main-container">
      <h1> Explorador de personajes de Game of Thrones</h1>

      <div className="paper">
        <form onChange={handleSearch} className="search-form">
          <input
            name="search"
            type="text"
            placeholder="Busqueda de personaje"
          ></input>
        </form>
      </div>

      <div className="characters-container">
        {displayedCharacters ? (
          displayedCharacters.map((value, index) => {
            console.log(value);
            return (
              <CharCard
                key={index}
                image={value.image}
                name={value.name}
                onClick={() => history.push(`/character?id=${value._id}`)}
              />
            );
          })
        ) : (
          <p>Cargando contenido...</p>
        )}
      </div>
      <div className="pagination-container">
        {displayedCharacters && (
          <Pagination
            activePage={actualPage}
            itemsCountPerPage={10}
            totalItemsCount={characters.length}
            pageRangeDisplayed={5}
            onChange={handlePageChange}
          />
        )}
      </div>
    </div>
  );
}

export default App;
