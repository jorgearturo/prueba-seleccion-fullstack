require('dotenv').config();

const config = {
  prod: process.env.PROD === 'true',
  port: process.env.PORT,
  uri: process.env.URI,
  dbName: process.env.DB_NAME,
};

module.exports = { config };
