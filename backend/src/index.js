const express = require('express');
const path = require('path');
const app = express();
const { mongoose } = require('./database');
const runUpdaters = require('./scripts/updaters');
const GotService = require('./services/GotUpdate');
const appRouter = require('./routes/');
const cors = require('cors');

app.options('*', cors());
app.use(cors());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  next();
});
app.set('port', process.env.PORT || 4000);
app.use(express.json());
app.use('/', appRouter);

app.listen(app.get('port'), () => {
  console.log(`Server on port ${app.get('port')}`);
  run();
});

async function run() {
  const gotService = new GotService();
  await gotService.startScript();
  runUpdaters();
}
