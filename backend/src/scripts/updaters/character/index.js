const cron = require('node-cron');
const GotUpdateService = require('../../../services/GotUpdate');
const cronExpressionInterface = require('../../../helpers/cronExpressionInterface');
const gotUpdateService = new GotUpdateService();

async function characterUpdater() {
  // Updates every 1 min by default
  const cronExpression = cronExpressionInterface('m1');

  cron.schedule(cronExpression, async () => {
    try {
      const update = await gotUpdateService.updateCharacters();
    } catch (error) {
      console.log(error);
      return error;
    }
  });
}

module.exports = characterUpdater;
