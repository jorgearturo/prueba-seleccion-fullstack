const MongoLib = require('../../database');
const fetch = require('node-fetch');
const objectHash = require('object-hash');
const ConfigModel = require('../../models/config');

class GotUpdateService {
  constructor() {
    this.mongoDB = new MongoLib();
    this.apiUrl = 'https://api.got.show/';
  }

  async startScript() {
    console.clear();
    console.log('[START] GOT character update script');
    if ((await this.ping()) && (await this.checkDbHealth())) {
      await this.updateCharacters();
      return true;
    } else {
      console.log('Failed to run start script...');
      throw false;
    }
  }

  async ping() {
    try {
      console.log('Testing connection with got.show API...');
      return true;
    } catch (error) {
      console.log('Ping GOT API error !', error);
      return error;
    }
  }

  async checkDbHealth() {
    try {
      console.log('Checking MongoDB...');
      const statCollectionExists = await this.mongoDB.collectionExists('STATS');
      if (!statCollectionExists) {
        await this.mongoDB.create('STATS', {
          _id: 'charData',
          charactersHash: 'default',
        });
      }
      // CHECK STATS INTEGRITY
      const characterCollectionExists = await this.mongoDB.collectionExists(
        'GOT_CHARACTERS'
      );
      if (!characterCollectionExists) {
        await this.mongoDB.createCollection('GOT_CHARACTERS');
        // CREATE COLLECTION
      }

      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async updateCharacters() {
    try {
      const actualCharacters = await this.fetchAllCharacters();
      if (await this.isUpdateRequired(actualCharacters)) {
        await this.update(actualCharacters);
      }
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async isUpdateRequired(data) {
    const { charactersHash } = await this.mongoDB.getByCustomId(
      'STATS',
      'charData'
    );
    const dataHash = objectHash(data);
    if (charactersHash === dataHash) {
      console.log('No updates found...');
      return false;
    }
    return true;
  }

  async update(characters) {
    console.log('Updating character database...');
    const updateObj = {
      $set: {
        _id: 'charData',
        charactersHash: objectHash(characters),
      },
    };
    const query = { _id: 'charData' };
    const updateHash = await this.mongoDB.updateOrCreate(
      'STATS',
      query,
      updateObj
    );
    const emptyColl = await this.mongoDB.emptyCollection('GOT_CHARACTERS');
    const pushColl = await this.mongoDB.insertMany(
      'GOT_CHARACTERS',
      characters
    );
  }
  async buildConfigModel(data) {
    const hash = objectHash(data);
    const model = new ConfigModel({ charactersHash: hash });
    model.save((err) => {
      if (err) {
        console.log(err);
        return err;
      }
    });
    return model;
  }

  async fetchAllCharacters() {
    try {
      const characters = new Promise((resolve, reject) => {
        fetch(this.apiUrl + '/api/general/characters')
          .then((res) => res.json())
          .then((data) => {
            if (data.book.length > 0) {
              resolve(data.book);
            }
            reject('No characters where found ');
          });
      });
      return await characters;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}

module.exports = GotUpdateService;
