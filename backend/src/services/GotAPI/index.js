const MongoLib = require('../../database');
const fetch = require('node-fetch');

class GotAPIService {
  constructor() {
    this.mongoDB = new MongoLib();
  }

  async getChar(id) {
    try {
      const character = await this.mongoDB.getByDefaultId('GOT_CHARACTERS', id);
      if (character) return character;
      throw 'Woops!, character not found. ';
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getCharacters() {
    try {
      const characters = await this.mongoDB.getAll('GOT_CHARACTERS');
      return characters;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}

module.exports = GotAPIService;
