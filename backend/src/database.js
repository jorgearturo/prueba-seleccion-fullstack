const mongoose = require('mongoose');
const { config } = require('./config');
const { MongoClient, ObjectId } = require('mongodb');

const URI = config.uri;
const DB_NAME = config.dbName;
const PROD = config.prod;

const MONGO_URI = () => {
  if (!PROD) {
    return URI;
  }
  //En produccion lo usuaría con docker, idealmente
  return `mongodb://mongodb:27017/`;
};

class MongoLib {
  constructor() {
    this.client = new MongoClient(MONGO_URI(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    this.dbName = 'MAIN_DB';
    this.connection = new Promise((resolve, reject) => {
      this.client.connect((error) => {
        if (error) {
          reject(error);
        } else {
          resolve(this.client.db(this.dbName));
        }
      });
    });
  }

  async collectionExists(collection) {
    return this.connection.then(async (db) => {
      return new Promise((resolve, reject) => {
        db.listCollections({ name: collection }).next(function (err, collinfo) {
          if (collinfo) {
            resolve(true);
          }
          resolve(false);
        });
      });
    });
  }

  async createIndex(collection, indexObject) {
    return this.connection.then((db) => {
      return new Promise((resolve, reject) => {
        db.collection(collection).createIndex({ time: 1 }, function (
          err,
          result
        ) {
          console.log(result);
        });
      });
    });
  }

  async createCollection(collection) {
    return this.connection.then((db) => {
      return new Promise((resolve, reject) => {
        db.createCollection(collection, (res, error) => {
          if (error) resolve(false);
          resolve(true);
        });
      });
    });
  }

  async findSort(collection, query, sort) {
    return this.connection.then((db) => {
      return db.collection(collection).find(query).sort(sort).toArray();
    });
  }

  getAll(collection, query) {
    return this.connection.then((db) => {
      return db.collection(collection).find(query).toArray();
    });
  }

  getAllInverted(collection, timestamp) {
    const query = { timestamp: { $gte: timestamp } };
    return this.connect().then((db) => {
      return db
        .collection(collection)
        .find(query)

        .limit(20)
        .toArray();
    });
  }

  getLastNElements(collection, nElements) {
    const nat = { _id: -1 };
    return this.connection.then((db) => {
      return db
        .collection(collection)
        .find()
        .sort(nat)
        .limit(nElements)
        .toArray();
    });
  }

  getByDefaultId(collection, id) {
    return this.connection.then((db) => {
      return db.collection(collection).findOne({ _id: ObjectId(id) });
    });
  }
  getByCustomId(collection, id) {
    return this.connection.then((db) => {
      return db.collection(collection).findOne({ _id: id });
    });
  }
  getByQuery(collection, query) {
    return this.connection.then((db) => {
      return db.collection(collection).find(query).toArray();
    });
  }

  find(collection, variable, content) {
    return this.connection.then((db) => {
      return db
        .collection(collection)
        .find({ variable: content })
        .toArray(function (err, data) {
          if (err) {
            console.log('Error:', err);
          } else {
            console.log(data);
            console.log('Data:', data);
          }
        });
    });
  }

  emptyCollection(collection) {
    return this.connection
      .then((db) => {
        return db.collection(collection).deleteMany({});
      })
      .then((result) => {
        return result;
      })
      .catch((error) => {
        return { error: 11000 };
      });
  }

  insertMany(collection, data) {
    return this.connection
      .then((db) => {
        return db.collection(collection).insertMany(data);
      })
      .then((result) => result)
      .catch((error) => {
        return { error: 11000 };
      });
  }
  create(collection, data) {
    return this.connection
      .then((db) => {
        return db.collection(collection).insertOne(data);
      })
      .then((result) => result.insertedId)
      .catch((error) => {
        return { error: 11000 };
      });
  }

  async updateOrCreate(collection, query, newValues) {
    try {
      return this.connection.then(async (db) => {
        await db
          .collection(collection)
          .updateOne(query, newValues, function (err, res) {
            if (err) throw err;
            if (res.matchedCount === 0) {
              db.collection(collection).insertOne(newValues.$set);
            }
          });
        return true;
      });
    } catch (e) {
      console.log(e);
      return e;
    }
  }
  filterDelete(collection, toDeleteIds) {
    return this.connection
      .then((db) => {
        return db.collection(collection).deleteMany({ source: toDeleteIds });
      })
      .then((result) => result);
  }

  delete(collection, id) {
    return this.connection
      .then((db) => {
        return db.collection(collection).deleteOne({ _id: ObjectId(id) });
      })
      .then((result) => result);
  }
}

module.exports = MongoLib;
