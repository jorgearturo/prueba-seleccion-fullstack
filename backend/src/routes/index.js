const express = require('express');
const api = express.Router();
const GotAPIService = require('../services/GotAPI');
const gotAPIService = new GotAPIService();

api.get('/characters', async function (req, res, next) {
  try {
    let id = req.query.id;
    if (id) {
      const character = await gotAPIService.getChar(id);
      res.send(character);
    } else {
      const characters = await gotAPIService.getCharacters();
      res.send(characters);
    }
  } catch (err) {
    next(err);
  }
});

module.exports = api;
