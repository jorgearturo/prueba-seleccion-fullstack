function cronExpressionInterface(key) {
  switch (key) {
    case 'd1':
      return '0 0 0 * * *';
      break;
    case 'h1':
      return '0 0 * * * *';
      break;
    case 'm1':
      return '* * * * *';
      break;
    case 's30':
      return '*/30 * * * * *';
      break;
    default:
      return false;
    // code block
  }
}

module.exports = cronExpressionInterface;
